(function () {
  'use strict';

  // We will simulate some delay when retrieving data...
  angular.module('wthAngularLibSeed.services').factory('wthDataService', ['$timeout', dataServiceFunction]);

  function dataServiceFunction($timeout) {

    var service = {
      getPeople: getPeople,
      getPerson: getPerson
    };

    // ----------------------------------------------------------------------------
    // Service methods
    // ----------------------------------------------------------------------------
    function getPeople() {
      return $timeout(function () {
        return people;
      }, 500);
    }

    function getPerson(id) {
      return $timeout(function () {
        for (var i = 0; i < people.length; i++) {
          if (people[i].id === id) {
            return people[i];
          }
        }
        return undefined;
      }, 300);
    }

    // ----------------------------------------------------------------------------
    // Helper methods
    // ----------------------------------------------------------------------------

    // ----------------------------------------------------------------------------
    // Mock Data
    // ----------------------------------------------------------------------------

    var people = [{
      id: 'jackey',
      name: 'Jackey Pantalones',
      profileImage: 'jackey.jpg',
      profileBackgroundImage: 'jackey.background.jpg',
      nickNames: ['Slacker'],
      birthday: new Date(new Date(1243814400000)),
      parents: []
    }, {
      id: 'troy',
      name: 'Jesus \'T\' Camisita',
      profileImage: 'troy.jpg',
      profileBackgroundImage: 'troy.background.jpg',
      nickNames: ['Troyito'],
      birthday: new Date(1243814400000),
      parents: []
    }, {
      id: 'lilly',
      name: 'Lilly Pantalones',
      profileImage: 'lilly.jpg',
      profileBackgroundImage: 'lilly.background.jpg',
      nickNames: ['Skinny Emo Girl'],
      birthday: new Date(1243814400000),
      parents: ['jackey', 'troy']
    }, {
      id: 'eva',
      name: 'Eva Pantalones',
      profileImage: 'eva.jpg',
      profileBackgroundImage: 'eva.background.jpg',
      nickName: ['Eva The Terrible'],
      birthday: new Date(1243814400000),
      parents: ['jackey', 'troy']
    }, {
      id: 'edi',
      name: 'Edith Heisenberg',
      profileImage: 'edi.jpg',
      profileBackgroundImage: 'edi.background.jpg',
      nickNames: [],
      birthday: new Date(1243814400000),
      parents: []
    }, {
      id: 'stuart',
      name: 'Stuart Heisenberg',
      profileImage: 'stuart.jpg',
      profileBackgroundImage: 'stuart.background.jpg',
      nickNames: [],
      birthday: new Date(1243814400000),
      parents: []
    }, {
      id: 'sequoia',
      name: 'Sequoia Heisenberg',
      profileImage: 'sequoia.jpg',
      profileBackgroundImage: 'sequoia.background.jpg',
      nickNames: [],
      birthday: new Date(1243814400000),
      parents: ['edi', 'stuart']
    }];

    return service;
  }
}());
