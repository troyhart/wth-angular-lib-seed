(function () {
  'use strict';
  // jshint -W030

  describe('Verify modules are loading', function () {

    var module;
    var dependencies = [];

    var hasModule = function (module) {
      return dependencies.indexOf(module) >= 0;
    };

    beforeEach(function () {

      // Get module
      module = angular.module('wthAngularLibSeed');
      dependencies = module.requires;
    });

    it('should load config module', function () {
      expect(hasModule('wthAngularLibSeed.config')).to.be.ok;
    });

    it('should load filters module', function () {
      expect(hasModule('wthAngularLibSeed.filters')).to.be.ok;
    });

    it('should load directives module', function () {
      expect(hasModule('wthAngularLibSeed.directives')).to.be.ok;
    });

    it('should load services module', function () {
      expect(hasModule('wthAngularLibSeed.services')).to.be.ok;
    });

  });
}());
