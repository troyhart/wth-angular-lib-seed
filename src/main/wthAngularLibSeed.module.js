(function () {
  'use strict';

  // Create all modules and define dependencies to make sure they exist
  // and are loaded in the correct order to satisfy dependency injection
  // before all nested files are concatenated by Gulp

  // Config
  angular.module('wthAngularLibSeed.config', [])
    .value('wthAngularLibSeed.config', {
      debug: true
    });

  // Modules
  angular.module('wthAngularLibSeed.directives', ['wthAngularLibSeed.services']);

  angular.module('wthAngularLibSeed.filters', [])
    .value('greetings', ['Hello'/* !!!NOTE: when multiple values are specified it freaks angular's digest cycle out and causes errors...so this filter is a bad idea from inception!!!, 'Bonjour', 'Howdy', '\'sup', 'Mir-dita', 'Salü', 'Shlomo', 'Salud', 'Tjike', '¡Hola!'*/]);

  angular.module('wthAngularLibSeed.services', []);

  angular.module('wthAngularLibSeed',
    [
      'wthAngularLibSeed.config',
      'wthAngularLibSeed.directives',
      'wthAngularLibSeed.filters',
      'wthAngularLibSeed.services'
    ]);
}());
