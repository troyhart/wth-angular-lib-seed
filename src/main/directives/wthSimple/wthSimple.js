(function () {
  'use strict';

  angular.module('wthAngularLibSeed.directives').directive('wthSimple', [directiveDef]);

  function directiveDef() {
    return {
      restrict: 'E',
      template: '<h1>How can I be more SIMPLE!?!?</h1>'
    };
  }
}());

