(function () {
  'use strict';
  // jshint -W040, -W117

  angular.module('wthAngularLibSeed.directives').directive('wthGuest', ['wthDataService', directiveDef]);

  function directiveDef(wthDataService) {
    return {
      restrict: 'E',
      scope: {
        name: '@'
      },
      templateUrl: '/src/main/directives/wthGuest/wthGuest.template.html',
      controllerAs: 'wthGuest',
      bindToController: true,
      controller: controllerFunction
    };

    function controllerFunction() {
      var vm = this;
      vm.person = {
        name: vm.name,
        profileImage: 'guest.jpg',
        profileBackgroundImage: 'guest.background.jpg'
      };
      wthDataService.getPerson(vm.name).then(function (data) {
        if (data) {
          vm.person = data;
        }
        console.log({person: vm.person});
      });
    }
  }
}());
