(function () {
  'use strict';

  describe('wthAngularLibSeed.directives module', function () {
    beforeEach(module('wthAngularLibSeed.directives'));

    describe('wthGuest directive with unknown guest name', function () {
      beforeEach(module(function ($provide) {
        // TODO: test soething...but wait, I don't know this testing api...
      }));
      it('it should have a default guest', inject(function (wthGuestDirective) {
        // TODO: test soething...but wait, I don't know this testing api...
      }));
    });
  });
}());
