// *************************************************************************************************
// WTF! What's up with the name `wthRandomGreetingFilter`? Where does this come from???
// The inject function argument in the tests below must follow the naming spec: <filterName>Filter
// See `withRandomGreeting.filter.js` for <filterName>: `... .filter('wthRandomGreeting', ...)`
// *************************************************************************************************

(function () {
  'use strict';

  describe('wthAngularLibSeed.filters module', function () {
    beforeEach(module('wthAngularLibSeed.filters'));

    describe('wthRandomGreeting filter with many possibilities', function () {
      beforeEach(module(function ($provide) {
        $provide.value('greetings', ['g1', 'g2', 'g3', 'g4', 'g5', 'g6', 'g7', 'g8', 'g9', 'g0']);
      }));
      it('should replace %GREETING%', inject(function (wthRandomGreetingFilter) {
        expect(wthRandomGreetingFilter('%GREETING%')).to.not.equal('%GREETING%');
      }));
    });

    describe('wthRandomGreeting filter with one possibility', function () {
      beforeEach(module(function ($provide) {
        $provide.value('greetings', ['g1']);
      }));
      it('should replace %GREETING%', inject(function (wthRandomGreetingFilter) {
        expect(wthRandomGreetingFilter('%GREETING%')).to.equal('g1');
      }));
    });
  });
}());
