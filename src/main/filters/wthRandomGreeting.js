(function () {
  'use strict';

  angular.module('wthAngularLibSeed.filters').filter('wthRandomGreeting', ['greetings', function (greetings) {
    return function (text) {
      var idx = Math.floor(Math.random() * greetings.length);
      return String(text).replace(/\%GREETING\%/mg, greetings[idx]);
    };
  }]);
}());
