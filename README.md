# wth-angular-lib-seed

A seed application for library projects.

The key to building the components in a library project involves `bower link`.
You will invoke this command from this project (with no arguments) to create an
OS level symbolic link. Then, in another project (an 'application' project) you
will invoke the command with the name of this project. See the bower documentation
for more information.

## Requirements

- Install node
- Install (global) node dependencies

### Install node

#### OSX

Install [home brew](http://brew.sh/) and use it to install node as shown below:
 
    $ brew install node
    
NOTE (highly recommended): you can alleviate the need to run as sudo by [following these instructions](http://jpapa.me/nomoresudo).

#### Windows

Install [chocolatey](https://chocolatey.org/) and use it to install node. You will need to open a command prompt as
`administrator` and execute the commands as shown below:

    $ choco install nodejs
    $ choco install nodejs.install
    
#### Linux

Use [nvm](https://github.com/creationix/nvm). This is also an option for the other platforms but I don't have the 
experience on those platforms to be able to recommend or not.

### Install (global) node dependencies

Open a terminal/command line and invoke `npm install -g` for the required global dependencies, as shown below:

    $ npm install -g node-inspector jscs jshint bower gulp

## Quick Start Development

    $ npm install
    $ gulp
    
## Build

This application implements a gulp build. You can get a listing of all available build tasks with `gulp help`. For the
general development usecase you will typically just enter `gulp` and start developing -- gulp will be in the background
watching and re-compiling/processing all the source. `gulp test-*` (replacing `*` with valid options listed by `gulp help`)
will build and test and exit -- i.e. it won't monitor. Any of `gulp`, `gulp build`, or `gulp test-*` will produce output
in `dist/`.

If you really want to know about the build, check the source. It's all encompassed in `gulpfile.js/`.

### Bower linking

In order to test this component as you are developing it you can use bower's `link` API. From this project root directory
invoke `bower link` (with no arguments) to setup a symbolic link to the root of the project. Then, in any (application)
project that has a dependency on this one you can invoke `bower link wth-angular-lib-seed` from that project's root
directory and that project will connect to the symbolic link.

## Dev Tools

### JSHint

We use JSHint for static code analysis. Configuration is found in `.jshintrc`. 
For configuration options see: [http://jshint.com/docs/options]()
 
### JSCS

We use JSCS for code verification. Configuration is found in `.jscsrc`. For
style rules see: [http://jscs.info/rules]()

### Editorconfig

We use Editorconfig to enforce editor configuration settings. Like indent style 
and size, charset, and other settings like trailing whitespace and final new 
line. Configuration is found in `.editorconfig`. For more information see: 
[http://editorconfig.org/]()

## Configure IDE

### webstorm

#### Editorconfig

Webstorm 10 comes with an Editorconfig plugin that will automatically look for a `.editorcongig` file in your
project. See plugin documentation for more details. Be sure to always tell webstorm to read configuration from
 your local file.

#### JSHint

Use "File | Settings | Languages and Frameworks | JavaScript | Code Quality Tools | JSHint" to enable JSHint
and configure it.

See jetbrains help for configuration options: https://www.jetbrains.com/webstorm/help/jshint.html

IMPORTANT: "Use config files"! Be sure to check this configuration option 
on the JSHint settings page. It should be next to the "Enable" checkbox.

Be sure to configure it to look for the local `.jshintrc` file.

NOTE, consider using "File | Default Settings | Languages and Frameworks | JavaScript | Code Quality Tools | JSHint"
to apply the JSHint "Use config files" configuration as the DEFAULT for webstorm.

#### JSCS

Use "File | Settings | Languages and Frameworks | JavaScript | Code Quality Tools | JSCS" to enable JSHint
and configure it. You can also apply this configuration as a DEFAULT via "File | Default Setting | ...".

See jetbrains help for configuration options: https://www.jetbrains.com/webstorm/help/jscs.html

Be sure to configure it to look for the local `.jscsrc` file.

## Credits
