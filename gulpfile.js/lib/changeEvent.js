'use strict';
//jshint node:true

var log = require(__dirname + '/../lib/log');
var config = require(__dirname + '/../config')();

var args = require('yargs').argv;

module.exports = changeEvent;

///////////////////////////////////

function changeEvent(event) {
  var srcPattern = new RegExp('/.*(?=/' + config.source + ')/');
  log('File ' + event.path.replace(srcPattern, '') + ' ' + event.type);
}
