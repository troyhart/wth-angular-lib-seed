'use strict';
//jshint node:true

var path = require('path');
var config = require(path.join(__dirname, '../config'))();

var karma = require('karma').server;

var log = require(path.join(__dirname, 'log'));

module.exports = startTests;

////////////////////

function startTests(concatenated, minified, done) {
  var child;
  var excludeFiles = [];//don't need yet! Maybe never will.
  var karmaConfig = config.karmaConfiguration;

  karmaConfig.concatenated = concatenated;
  karmaConfig.minified = minified;

  karma.start({
    configFile: karmaConfig.file,
    exclude: excludeFiles,
    // our build script watch will kick off the tests on change, so we don't need karma to monitor...
    singleRun: true
  }, karmaCompleted);

  function karmaCompleted(karmaResult) {
    log('Karma completed!');
    if (child) {
      log('Shutting down the child process');
      child.kill();
    }
    if (karmaResult === 1) {
      done('karma: tests failed with code ' + karmaResult);
    } else {
      done();
    }
  }
}
