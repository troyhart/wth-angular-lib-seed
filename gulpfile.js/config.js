'use strict';
// jshint node:true

var path = require('path');
var camelize = require('camelize');
var args = require('yargs').argv;
var log = require(path.join(__dirname, 'lib/log'));

// Root directory
var rootDirectory = path.resolve('.');

// Source directory for build process
var sourceDirectory = path.join(rootDirectory, 'src');
// Temporary output directory for processed source
var tempDirectory = path.join(rootDirectory, '.tmp');
// Build reporting output directory
var reportDirectory = path.join(rootDirectory, 'build_reports');

// Package configuration
var pkg = require(path.join(rootDirectory, 'package.json'));

var testHelperSourceFiles = 'src/test/helpers/**/*.js';
var specsSourceFiles = 'src/test/unit/**/*.spec.js';

// All component javascript source ordered so that modules are first.
// This source includes all test source too (../src/**/*.spec.js).
var allCompJSSourceFiles = [
  // Make sure module files are handled first
  'src/main/**/*.module.js',
  // Then add all the rest of the JavaScript files
  'src/main/**/*.js'
];

// A reference to all the component's template html source.
var htmlTemplateFiles = 'src/main/**/*.template.html';

// less source files
var lessFiles = 'src/main/**/*.less';

// Files for linting
var lintOnlyFiles = [
  // all root level javascript files
  '*.js',
  // all gulp build javascript files
  'gulpfile.js/**/*.js'
];

// Configuration for the templateCache processing.
var templateCache = {
  file: 'templates.js',
  options: {
    module: camelize(pkg.name),
    standalone: false,
    root: 'src/main/'
  }
};

var karmaConfiguration = {file: path.join(rootDirectory, 'karma.conf.js'), concatenated: false, minified: false};

module.exports = function () {
  return {
    rootDirectory: rootDirectory,
    sourceDirectory: sourceDirectory,
    tempDirectory: tempDirectory,
    testHelperSourceFiles: testHelperSourceFiles,
    specsSourceFiles: specsSourceFiles,
    allCompJSSourceFiles: allCompJSSourceFiles,
    lessFiles: lessFiles,
    lintOnlyFiles: lintOnlyFiles,
    htmlTemplateFiles: htmlTemplateFiles,
    templateCache: templateCache,
    packageDescriptorFiles: [
      path.join(rootDirectory, 'package.json'),
      path.join(rootDirectory, 'bower.json')
    ],
    karmaConfiguration: karmaConfiguration,
    karmaOptions: getKarmaOptions
  };
};

////////////////////////////////////////////////////////////////////////////////
// Helper Funcitons
////////////////////////////////////////////////////////////////////////////////

/**
 * @Param {Boolean} concatenated - Optional flag that will cause the configuration to specify the optimized
 *    distribution artifact (the concatenated .js file)
 * @Param {Boolean} minified - Optional flag that will cause the configuration to specify the optimized and minified
 *    distribution artifact (the concatenated .min.js file). NOTE: this is only considered when concatenated is `true`
 *
 */
function getKarmaOptions() {
  var bowerrc = requireJSONWithNonJSONExtension(path.join(rootDirectory, '.bowerrc'));
  var bowerFiles = require('wiredep')({
    directory: path.join(rootDirectory, bowerrc.directory),
    bowerJson: require(path.join(rootDirectory, 'bower.json')),
    dependencies: true,
    devDependencies: true
  })['js'];

  var files = [].concat(
    bowerFiles,
    testHelperSourceFiles,
    specsSourceFiles
  );

  var buildType = 'src';
  // Will test optimized source -- either minified or just concatenated
  if (karmaConfiguration.concatenated) {
    buildType = 'dist/'.concat(pkg.name).concat(karmaConfiguration.minified ? '.min.js' : '.js');
    files = files.concat(
      // add the 'compiled' distribution source
      buildType
    );
  } else {
    files = files.concat(
      // add the 'raw' source js
      allCompJSSourceFiles,
      // add the 'compiled' html templates
      path.join(tempDirectory, templateCache.file));
  }

  if (args.verbose) {
    log({getKarmaOptions: 'testFiles -->> ' + files});
  }

  var options = {
    files: files,
    exclude: [],
    coverage: {
      dir: path.join(reportDirectory, buildType + '/coverage'),
      reporters: [
        {type: 'html', subdir: 'report-html'},
        {type: 'lcov', subdir: 'report-lcov'},// can be used to integrate code coverage into jenkins
        {type: 'text-summary'}
      ]
    },
    preprocessors: {}
  };
  if (karmaConfiguration.concatenated) {
    options.preprocessors[buildType] = ['coverage'];
  } else {
    options.preprocessors['src/main/**/*.js'] = ['coverage'];
  }

  return options;
}

function requireJSONWithNonJSONExtension(filePath) {
  var fs = require('fs');
  return JSON.parse(fs.readFileSync(filePath, 'utf8'));
}
