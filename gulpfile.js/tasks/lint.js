'use strict';
// jshint node:true
// jshint -W024

var path = require('path');
var config = require(path.join(__dirname, '../config'))();

var gulp = require('gulp');
var args = require('yargs').argv;
var $ = require('gulp-load-plugins')({lazy: true});

///////////////////////////////////////////////////////////////////////////
// Tasks
///////////////////////////////////////////////////////////////////////////

/**
 * Validate ALL javascript source
 */
gulp.task('vet', function () {
  return gulp.src(
    [].concat(
      config.lintOnlyFiles,
      config.testHelperSourceFiles,
      config.specsSourceFiles,
      config.allCompJSSourceFiles
    ))
    .pipe($.plumber())
    .pipe($.if(args.verbose, $.print()))
    .pipe($.jshint())
    .pipe($.jscs())
    .pipe($.jshint.reporter('jshint-stylish'))
    .pipe($.jshint.reporter('fail'));
});
