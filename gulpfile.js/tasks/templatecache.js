'use strict';
// jshint node:true
// jshint -W024

var path = require('path');
var config = require(path.join(__dirname, '../config'))();
var log = require(path.join(__dirname, '../lib/log'));

var gulp = require('gulp');
var del = require('del');
var args = require('yargs').argv;
var $ = require('gulp-load-plugins')({lazy: true});

///////////////////////////////////////////////////////////////////////////
// Tasks
///////////////////////////////////////////////////////////////////////////

gulp.task('clean-templatecache', function (done) {
  del(path.join(config.tempDirectory, config.templateCache.file)).then(function (paths) {
    // execute the callback because we are done!
    if (args.verbose) {
      log({'clean-templatecache': 'paths -> ' + paths});
    }
    done();
  });
});

gulp.task('templatecache', ['clean-templatecache'], function () {
  return gulp.src(config.htmlTemplateFiles)
    .pipe($.if(args.verbose, $.print()))
    .pipe($.minifyHtml({empty: true}))
    .pipe($.angularTemplatecache(
      config.templateCache.file,
      config.templateCache.options
    ))
    .pipe(gulp.dest(config.tempDirectory));
});
