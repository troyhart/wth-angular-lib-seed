'use strict';
// jshint node:true
// jshint -W024

var path = require('path');

var log = require(path.join(__dirname, '../lib/log'));
var clean = require(path.join(__dirname, '../lib/clean'));
var config = require(path.join(__dirname, '../config'))();

var gulp = require('gulp');
var args = require('yargs').argv;
var $ = require('gulp-load-plugins')({lazy: true});

gulp.task('css', ['clean-css'], function () {
  log('Compiling Less --> CSS');

  return gulp
    .src(config.lessFiles)
    .pipe($.plumber())
    .pipe($.if(args.verbose, $.print()))
    .pipe($.less())
    .pipe($.autoprefixer({browsers: ['last 2 version', '> 5%']}))
    .pipe(gulp.dest(config.tempDirectory));
});

gulp.task('clean-css', function (done) {
  clean(config.temp + '**/*.css', done);
});
