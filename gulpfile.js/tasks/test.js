'use strict';
// jshint node:true

var path = require('path');
var config = require(path.join(__dirname, '../config'))();

var log = require(path.join(__dirname, '../lib/log'));
var startTests = require(path.join(__dirname, '../lib/startTests'));

var gulp = require('gulp');
var $ = require('gulp-load-plugins')({lazy: true});
var runSequence = require('run-sequence').use(gulp);

///////////////////////////////////////////////////////////////////////////
// Tasks
///////////////////////////////////////////////////////////////////////////

/**
 * Run test once and exit
 */
gulp.task('test-src', function (done) {
  startTests(false, false, done);
});

gulp.task('test-dist', function (done) {
  runSequence('test-dist-concatenated', 'test-dist-minified', done);
});

/**
 * Run test once and exit
 */
gulp.task('test-dist-concatenated', function (done) {
  startTests(true, false, done);
});

/**
 * Run test once and exit
 */
gulp.task('test-dist-minified', function (done) {
  startTests(true, true, done);
});
