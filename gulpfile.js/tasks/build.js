'use strict';
// jshint node:true
// jshint -W024

var path = require('path');
var config = require(path.join(__dirname, '../config'))();
var pkg = require(path.join(config.rootDirectory, 'package.json'));
var clean = require(path.join(__dirname, '/../lib/clean'));
var log = require(path.join(__dirname, '/../lib/log'));

var gulp = require('gulp');
var args = require('yargs').argv;
var runSequence = require('run-sequence').use(gulp);
var $ = require('gulp-load-plugins')({lazy: true});

///////////////////////////////////////////////////////////////////////////
// Tasks
///////////////////////////////////////////////////////////////////////////

gulp.task('build', ['clean-build', 'templatecache', 'css'], function (done) {
  return runSequence('test-src', 'optimize', done);
});

gulp.task('clean-build', function () {
  clean(path.join(config.rootDirectory, 'dist/**/*'));
});

gulp.task('optimize', function (done) {
  // This task is not intended to be executed directly...use build
  return runSequence('optimize-js', 'optimize-css', done);
});

gulp.task('optimize-js', function (done) {
  // This task is not intended to be executed directly...use build
  return gulp.src(
    [].concat(
      config.allCompJSSourceFiles,
      path.join(config.tempDirectory, config.templateCache.file)
    ))
    .pipe($.angularFilesort())
    .pipe($.plumber())
    .pipe($.if(args.verbose, $.print(function (filepath) {
      return 'Input Source: ' + filepath;
    })))
    .pipe($.concat(pkg.name + '.js'))
    .pipe(gulp.dest('./dist/'))
    .pipe($.if(args.verbose, $.print(function (filepath) {
      return 'Concatenated Source: ' + filepath;
    })))
    .pipe($.uglify())
    .pipe($.rename(pkg.name + '.min.js'))
    .pipe($.if(args.verbose, $.print(function (filepath) {
      return 'Minified Source: ' + filepath;
    })))
    .pipe(gulp.dest('./dist'));
});

gulp.task('optimize-css', function (done) {
  // This task is not intended to be executed directly...use build
  return gulp.src(
    [].concat(
      path.join(config.tempDirectory, '**/*.css')
    ))
    .pipe($.plumber())
    .pipe($.if(args.verbose, $.print(function (filepath) {
      return 'Input Source: ' + filepath;
    })))
    .pipe($.concat(pkg.name + '.css'))
    .pipe(gulp.dest('./dist/'))
    .pipe($.if(args.verbose, $.print(function (filepath) {
      return 'Concatenated Source: ' + filepath;
    })))
    .pipe($.csso())
    .pipe($.rename(pkg.name + '.min.css'))
    .pipe($.if(args.verbose, $.print(function (filepath) {
      return 'Minified Source: ' + filepath;
    })))
    .pipe(gulp.dest('./dist'));
});
