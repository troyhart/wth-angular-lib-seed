'use strict';
// jshint node:true

var path = require('path');
var config = require(path.join(__dirname, '../config'))();
var clean = require(path.join(__dirname, '/../lib/clean'));

var gulp = require('gulp');
var runSequence = require('run-sequence').use(gulp);
var $ = require('gulp-load-plugins')({lazy: true});

///////////////////////////////////////////////////////////////////////////
// Tasks
///////////////////////////////////////////////////////////////////////////

gulp.task('help', $.taskListing);

gulp.task('default', function () {
  runSequence('vet', 'build', 'watch');
});

gulp.task('clean', function (done) {
  clean([].concat(config.tempDirectory, path.join(config.rootDirectory, 'dist'), path.join(config.rootDirectory, 'build_reports')));
});

/**
 * Watch task
 */
gulp.task('watch', function () {
  // Watch all processable source
  gulp.watch(
    [].concat(
      config.allCompJSSourceFiles,
      config.htmlTemplateFiles,
      config.testHelperSourceFiles,
      config.specsSourceFiles,
      config.lessFiles
    ),
    ['build']
  );
  gulp.watch(config.lintOnlyFiles, ['vet']);
});
