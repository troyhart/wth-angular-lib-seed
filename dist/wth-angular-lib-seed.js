(function () {
  'use strict';

  // Create all modules and define dependencies to make sure they exist
  // and are loaded in the correct order to satisfy dependency injection
  // before all nested files are concatenated by Gulp

  // Config
  angular.module('wthAngularLibSeed.config', [])
    .value('wthAngularLibSeed.config', {
      debug: true
    });

  // Modules
  angular.module('wthAngularLibSeed.directives', ['wthAngularLibSeed.services']);

  angular.module('wthAngularLibSeed.filters', [])
    .value('greetings', ['Hello'/* !!!NOTE: when multiple values are specified it freaks angular's digest cycle out and causes errors...so this filter is a bad idea from inception!!!, 'Bonjour', 'Howdy', '\'sup', 'Mir-dita', 'Salü', 'Shlomo', 'Salud', 'Tjike', '¡Hola!'*/]);

  angular.module('wthAngularLibSeed.services', []);

  angular.module('wthAngularLibSeed',
    [
      'wthAngularLibSeed.config',
      'wthAngularLibSeed.directives',
      'wthAngularLibSeed.filters',
      'wthAngularLibSeed.services'
    ]);
}());

'use strict';

/*
 *  Phantom.js does not support Function.prototype.bind (at least not before v.2.0
 *  That's just crazy. Everybody supports bind.
 *  Read about it here: https://groups.google.com/forum/#!msg/phantomjs/r0hPOmnCUpc/uxusqsl2LNoJ
 *  This polyfill is copied directly from MDN
 *  https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind#Compatibility
 */
if (!Function.prototype.bind) {
  // jshint freeze: false

  Function.prototype.bind = function (oThis) {
    if (typeof this !== 'function') {
      // closest thing possible to the ECMAScript 5
      // internal IsCallable function
      var msg = 'Function.prototype.bind - what is trying to be bound is not callable';
      throw new TypeError(msg);
    }

    var aArgs = Array.prototype.slice.call(arguments, 1),
      fToBind = this,
      FuncNoOp = function () {
      },
      fBound = function () {
        return fToBind.apply(this instanceof FuncNoOp && oThis ? this : oThis,
          aArgs.concat(Array.prototype.slice.call(arguments)));
      };

    FuncNoOp.prototype = this.prototype;
    fBound.prototype = new FuncNoOp();

    return fBound;
  };
}

(function () {
  'use strict';

  angular.module('wthAngularLibSeed.filters').filter('wthRandomGreeting', ['greetings', function (greetings) {
    return function (text) {
      var idx = Math.floor(Math.random() * greetings.length);
      return String(text).replace(/\%GREETING\%/mg, greetings[idx]);
    };
  }]);
}());

(function () {
  'use strict';

  // We will simulate some delay when retrieving data...
  angular.module('wthAngularLibSeed.services').factory('wthDataService', ['$timeout', dataServiceFunction]);

  function dataServiceFunction($timeout) {

    var service = {
      getPeople: getPeople,
      getPerson: getPerson
    };

    // ----------------------------------------------------------------------------
    // Service methods
    // ----------------------------------------------------------------------------
    function getPeople() {
      return $timeout(function () {
        return people;
      }, 500);
    }

    function getPerson(id) {
      return $timeout(function () {
        for (var i = 0; i < people.length; i++) {
          if (people[i].id === id) {
            return people[i];
          }
        }
        return undefined;
      }, 300);
    }

    // ----------------------------------------------------------------------------
    // Helper methods
    // ----------------------------------------------------------------------------

    // ----------------------------------------------------------------------------
    // Mock Data
    // ----------------------------------------------------------------------------

    var people = [{
      id: 'jackey',
      name: 'Jackey Pantalones',
      profileImage: 'jackey.jpg',
      profileBackgroundImage: 'jackey.background.jpg',
      nickNames: ['Slacker'],
      birthday: new Date(new Date(1243814400000)),
      parents: []
    }, {
      id: 'troy',
      name: 'Jesus \'T\' Camisita',
      profileImage: 'troy.jpg',
      profileBackgroundImage: 'troy.background.jpg',
      nickNames: ['Troyito'],
      birthday: new Date(1243814400000),
      parents: []
    }, {
      id: 'lilly',
      name: 'Lilly Pantalones',
      profileImage: 'lilly.jpg',
      profileBackgroundImage: 'lilly.background.jpg',
      nickNames: ['Skinny Emo Girl'],
      birthday: new Date(1243814400000),
      parents: ['jackey', 'troy']
    }, {
      id: 'eva',
      name: 'Eva Pantalones',
      profileImage: 'eva.jpg',
      profileBackgroundImage: 'eva.background.jpg',
      nickName: ['Eva The Terrible'],
      birthday: new Date(1243814400000),
      parents: ['jackey', 'troy']
    }, {
      id: 'edi',
      name: 'Edith Heisenberg',
      profileImage: 'edi.jpg',
      profileBackgroundImage: 'edi.background.jpg',
      nickNames: [],
      birthday: new Date(1243814400000),
      parents: []
    }, {
      id: 'stuart',
      name: 'Stuart Heisenberg',
      profileImage: 'stuart.jpg',
      profileBackgroundImage: 'stuart.background.jpg',
      nickNames: [],
      birthday: new Date(1243814400000),
      parents: []
    }, {
      id: 'sequoia',
      name: 'Sequoia Heisenberg',
      profileImage: 'sequoia.jpg',
      profileBackgroundImage: 'sequoia.background.jpg',
      nickNames: [],
      birthday: new Date(1243814400000),
      parents: ['edi', 'stuart']
    }];

    return service;
  }
}());

(function () {
  'use strict';
  // jshint -W040, -W117

  angular.module('wthAngularLibSeed.directives').directive('wthGuest', ['wthDataService', directiveDef]);

  function directiveDef(wthDataService) {
    return {
      restrict: 'E',
      scope: {
        name: '@'
      },
      templateUrl: '/src/main/directives/wthGuest/wthGuest.template.html',
      controllerAs: 'wthGuest',
      bindToController: true,
      controller: controllerFunction
    };

    function controllerFunction() {
      var vm = this;
      vm.person = {
        name: vm.name,
        profileImage: 'guest.jpg',
        profileBackgroundImage: 'guest.background.jpg'
      };
      wthDataService.getPerson(vm.name).then(function (data) {
        if (data) {
          vm.person = data;
        }
        console.log({person: vm.person});
      });
    }
  }
}());

(function () {
  'use strict';

  angular.module('wthAngularLibSeed.directives').directive('wthSimple', [directiveDef]);

  function directiveDef() {
    return {
      restrict: 'E',
      template: '<h1>How can I be more SIMPLE!?!?</h1>'
    };
  }
}());


angular.module("wthAngularLibSeed").run(["$templateCache", function($templateCache) {$templateCache.put("/src/main/directives/wthGuest/wthGuest.template.html","<div>{{\'%GREETING%\' | wthRandomGreeting}} {{wthGuest.person.name}}</div>");}]);